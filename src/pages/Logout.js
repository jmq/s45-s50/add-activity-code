/*
* Filename: Logout.js
* Role:     Page
*/

import {Navigate} from 'react-router-dom';
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'


export default function Logout(){

	const { unsetUser, setUser } = useContext(UserContext)
	// localStorage.clear()

	unsetUser()

	// The moment this page loads, 
	// This useEffect is automatically run
	useEffect(() => {
		setUser({
			id: null
		})
	}, [])

	return(
		<Navigate to="/login" />
		)
}