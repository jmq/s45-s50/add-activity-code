/*
* Filename: PageNotFound.js
* Role:     Page
*/


import React from 'react'
import Banner from '../components/Banner'

function PageNotFound(props) {
	return (
		<Banner isError = {true} />
	)
}

export default PageNotFound