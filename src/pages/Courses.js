/*
* Filename: Course.js
* Role:     Page
*/


import PropTypes from 'prop-types'
import {Fragment, useEffect, useState} from 'react'
import CourseCard from '../components/CourseCard'
// import coursesData from '../data/coursesData'
import {Container} from 'react-bootstrap'

export default function Courses() {	
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course} />
	// 	)		
	// })

	//State that will be used to store the courses retrieved
	//from the database
	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch("http://localhost:4000/courses")
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	}, [])

	return (		
		<Fragment>
			{courses}
		</Fragment>		
	)
}

CourseCard.prototype = {
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string,
		price: PropTypes.number.isRequired
	})
}