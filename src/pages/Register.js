import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import {Navigate, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function Register (){

  const navigate = useNavigate()

  const {user, setUser} = useContext(UserContext);
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNo, setMobileNo] = useState('')
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [isActive, setIsAvtive] = useState(false)
  

  function registerUser(e) {

    e.preventDefault()

    fetch('http://localhost:4000/users/checkEmail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',        
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res => res.json())
      .then(data => {  
        if(data) {
          Swal.fire({
            title: 'Duplicate email found',
            icon: 'error',
            text: 'Please try another email address'
          })          
      } else {
        fetch('http://localhost:4000/users/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {

          if (data) {
            Swal.fire({
                title: "Registration Sucess!", 
                icon: "success",
                text: "You have successfully registered!"
            })

            setEmail('')
            setFirstName('')
            setLastName('')
            setMobileNo('')
            setPassword1('')
            setPassword2('')

            navigate("/login")
        } else {
            Swal.fire({
                title: "Something went wrong!",
                icon: "error", 
                text: "Please try again."
            })
        }



        })
      }
    })

  }

  useEffect(() => {
    if(
        (firstName !== '' && 
        lastName !== '' && 
        mobileNo.length === 11 && 
        email !== '' && 
        password1 !== '' && 
        password2 !== '') && 
        (password1 === password2)
      ) 
    {
      setIsAvtive(true)
    } else {
      setIsAvtive(false)
    }
  }, [firstName, lastName, mobileNo, email, password1, password2])

  return (
    (user.id !== null) ? 
      <Navigate to ="/courses" />
    :     
    <Form onSubmit = {(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="First Name"
            value = {firstName}
            onChange = {e => setFirstName(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="First Name"
            value = {lastName}
            onChange = {e => setLastName(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control 
            type="text" 
            placeholder="First Name"
            value = {mobileNo}
            onChange = {e => setMobileNo(e.target.value)}
            required />        
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
            type="email" 
            placeholder="Enter email"
            value = {email}
            onChange = {e => setEmail(e.target.value)}
            required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password1}
            onChange={ e => setPassword1(e.target.value)}
            required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
            type="password" 
            placeholder="Verify Password" 
            value={password2}
            onChange={ e => setPassword2(e.target.value)}
            required />
      </Form.Group>
      
      {
        isActive ? 
          <Button variant="primary" type="submit" id="submitBtn">
            Register
          </Button>
          :
          <Button variant="danger" type="submit" id="submitBtn" disabled>
            Register
          </Button>
      }

    </Form>
  )
}
