/*
* Filename: CourseCard.js
* Role:     Component
*/

// import {useState, useEffect} from 'react'
import {Button, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {
	
	const {name, description, price, _id} = courseProp
	//console.log(courseProp)
	// /*
	// 	Syntax:
	// 		const [getter, setter] = useState(initialGetterValue)
	// */
	// const [count, setCount] = useState(0)
	// const [seat, setSeat] = useState(30)

	// function enroll() {
	// 	setCount(count + 1)
	// 	setSeat(seat -1)
		
	// }

	// useEffect(() => {
	// 	if (seat === 0) {
	// 		alert ('No more seats available.')
	// 	}
	// }, [seat])

	return (
		<Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
{/*                <Card.Text>Enrollees: {count} </Card.Text>
                <Card.Text>Seats: {seat} </Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
    	</Card>
	)
}

